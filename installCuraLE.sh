#!/bin/bash

# Script was made using the useful info found at (https://forum.lulzbot.com/t/dependency-issues-installing-cura-lulzbot-on-ubuntu-20-04/22010/10)
# This script will install the neccesary dependencies needed to install Cura LE on an ubuntu based system.
# Script must be executed as root.
# Much thanks to HarlemSquirrel from the forum for the help :)    


# Make directory to store .deb's
mkdir cura-lulzbot-ubuntu-stuffs

# Go into that directory
cd cura-lulzbot-ubuntu-stuffs


# Install Dependencies (There may be more on your system, if there are, add them here before sending this to someone else)
sudo apt update
sudo apt install -y wget libgl1-mesa-glx libtinfo5 libgfortran4


# From: (https://www.lulzbot.com/learn/tutorials/cura-lulzbot-edition-installation-ubuntu)
# Download cura-lulzbot_3.2.21
wget --progress=bar:force:noscroll https://gitlab.com/lulzbot3d/cura-le/cura-lulzbot/uploads/0676b39295476b93181fa8a512f34265/cura-lulzbot_3.2.21_amd64.deb


# From: (https://packages.ubuntu.com/) 
# Download libgfortran3_6.4.0-17ubuntu1
wget --progress=bar:force:noscroll http://lug.mtu.edu/ubuntu/pool/universe/g/gcc-6/libgfortran3_6.4.0-17ubuntu1_amd64.deb

# Download libssl1.0.0_1.0.2n-1ubuntu5.6
wget --progress=bar:force:noscroll http://security.ubuntu.com/ubuntu/pool/main/o/openssl1.0/libssl1.0.0_1.0.2n-1ubuntu5.6_amd64.deb

# Download gcc-6-base_6.4.0-17ubuntu1
wget --progress=bar:force:noscroll http://lug.mtu.edu/ubuntu/pool/universe/g/gcc-6/gcc-6-base_6.4.0-17ubuntu1_amd64.deb

# Download libreadline7_7.0-3
wget --progress=bar:force:noscroll http://lug.mtu.edu/ubuntu/pool/main/r/readline/libreadline7_7.0-3_amd64.deb


# Install all .deb's
sudo dpkg -i *.deb; echo "Cura Lulzbot edition is now successfully installed!"
