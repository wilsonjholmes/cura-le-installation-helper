# Cura LE Installation Helper

Hunting for old dependencies to install Cura LE on any Ubuntu based distro newer than Bionic (18.04LTS) is a waste of time, when a script can do it for you :)